// generated by Fast Light User Interface Designer (fluid) version 1.0302

#ifndef net_ui_h
#define net_ui_h
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include "my_UI.h"
#include <FL/Fl_Menu_Bar.H>
extern Fl_Menu_Bar *mnu_bar;
extern void cb_mnuOpen(Fl_Menu_*, void*);
extern void cb_mnuExit(Fl_Menu_*, void*);
extern void cb_mnuEditor(Fl_Menu_*, void*);
extern void cb_mnuConfig(Fl_Menu_*, void*);
extern void cb_mnuHelpContent(Fl_Menu_*, void*);
extern void cb_mnuAbout(Fl_Menu_*, void*);
extern my_UI *myUI;
#include <FL/Fl_Box.H>
extern Fl_Box *txtNcallins;
#include <FL/Fl_Group.H>
extern Fl_Group *net_grp1;
extern Fl_Box *ptr_left;
extern Fl_Box *ptr_right;
extern Fl_Group *net_grp2;
extern Fl_Box *txtTitles;
extern Fl_Box *txtLine[15];
extern Fl_Group *dbSelectGroup;
extern Fl_Box *inpLoginSuffix;
extern Fl_Box *inpLoginPrefix;
extern Fl_Box *inpLoginArea;
extern Fl_Group *net_grp3;
extern Fl_Box *txtPick[10];
extern Fl_Box *txtPickArrows;
extern Fl_Box *bx_suffix;
extern Fl_Box *bx_prefix;
extern Fl_Box *bx_area;
extern Fl_Box *txtInfo;
Fl_Double_Window* newNetControl();
extern Fl_Menu_Item menu_mnu_bar[];
#define mnuOpen (menu_mnu_bar+1)
#define mnuExit (menu_mnu_bar+2)
#define mnuEditor (menu_mnu_bar+4)
#define mnuConfig (menu_mnu_bar+5)
#define mnuLogIns (menu_mnu_bar+6)
#define mnuSize (menu_mnu_bar+7)
#define mnu_Content (menu_mnu_bar+9)
#define mnu_About (menu_mnu_bar+10)
#include <FL/Fl_Tabs.H>
extern Fl_Tabs *tabsConfig;
#include <FL/Fl_Check_Button.H>
extern Fl_Check_Button *btn_new_login_is_up;
extern Fl_Check_Button *btnOpenEditor;
extern Fl_Check_Button *btn_current_call_in_is_up;
extern Fl_Group *tabGroupColors;
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>
extern Fl_Output *txtSample[5];
extern Fl_Button *btnFg[5];
extern Fl_Button *btnBg[5];
extern Fl_Group *tabGroupPriority;
#include <FL/Fl_Input.H>
extern Fl_Input *cfgP1;
extern Fl_Input *inpStatesList1;
extern Fl_Input *cfgP2;
extern Fl_Input *inpStatesList2;
extern Fl_Input *cfgP3;
extern Fl_Input *inpStatesList3;
extern Fl_Check_Button *chkAutoPriority;
#include <FL/Fl_Return_Button.H>
extern void cb_btnCloseConfig(Fl_Return_Button*, void*);
extern Fl_Return_Button *btnConfigOK;
Fl_Double_Window* configDialog();
#include <FL/Fl_Browser.H>
extern Fl_Browser *log_in_view;
extern Fl_Button *btn_copy_to_clipboard;
extern Fl_Button *btn_close_log_ins;
Fl_Double_Window* Log_ins_dialog();
#endif
