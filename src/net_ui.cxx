// generated by Fast Light User Interface Designer (fluid) version 1.0302

#include "net_ui.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <FL/fl_show_colormap.H>
#include "netsupport.h"
#include "netshared.h"
#include "config.h"
#include "net_config.h"

Fl_Menu_Bar *mnu_bar=(Fl_Menu_Bar *)0;

static void cb_mnuLogIns(Fl_Menu_*, void*) {
  open_log_ins();
}

static void cb_mnuSize(Fl_Menu_*, void*) {
  change_size();
}

Fl_Menu_Item menu_mnu_bar[] = {
 {"&Files", 0,  0, 0, 64, FL_NORMAL_LABEL, 0, 14, 0},
 {"&Open", 0,  (Fl_Callback*)cb_mnuOpen, 0, 128, FL_NORMAL_LABEL, 0, 14, 0},
 {"&Exit", 0,  (Fl_Callback*)cb_mnuExit, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {0,0,0,0,0,0,0,0,0},
 {"&Editor", 0,  (Fl_Callback*)cb_mnuEditor, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {"Config", 0,  (Fl_Callback*)cb_mnuConfig, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {"Log-Ins", 0,  (Fl_Callback*)cb_mnuLogIns, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {"Size", 0,  (Fl_Callback*)cb_mnuSize, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {"&Help", 0,  0, 0, 64, FL_NORMAL_LABEL, 0, 14, 0},
 {"Content", 0,  (Fl_Callback*)cb_mnuHelpContent, 0, 128, FL_NORMAL_LABEL, 0, 14, 0},
 {"About", 0,  (Fl_Callback*)cb_mnuAbout, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
 {0,0,0,0,0,0,0,0,0},
 {0,0,0,0,0,0,0,0,0}
};

my_UI *myUI=(my_UI *)0;

Fl_Box *txtNcallins=(Fl_Box *)0;

Fl_Group *net_grp1=(Fl_Group *)0;

Fl_Box *ptr_left=(Fl_Box *)0;

Fl_Box *ptr_right=(Fl_Box *)0;

Fl_Group *net_grp2=(Fl_Group *)0;

Fl_Box *txtTitles=(Fl_Box *)0;

Fl_Box *txtLine[15]={(Fl_Box *)0};

Fl_Group *dbSelectGroup=(Fl_Group *)0;

Fl_Box *inpLoginSuffix=(Fl_Box *)0;

Fl_Box *inpLoginPrefix=(Fl_Box *)0;

Fl_Box *inpLoginArea=(Fl_Box *)0;

Fl_Group *net_grp3=(Fl_Group *)0;

Fl_Box *txtPick[10]={(Fl_Box *)0};

Fl_Box *txtPickArrows=(Fl_Box *)0;

Fl_Box *bx_suffix=(Fl_Box *)0;

Fl_Box *bx_prefix=(Fl_Box *)0;

Fl_Box *bx_area=(Fl_Box *)0;

Fl_Box *txtInfo=(Fl_Box *)0;

Fl_Double_Window* newNetControl() {
  Fl_Double_Window* w;
  { Fl_Double_Window* o = new Fl_Double_Window(390, 455, "flnet");
    w = o;
    o->box(FL_BORDER_BOX);
    { mnu_bar = new Fl_Menu_Bar(0, 0, 390, 25);
      mnu_bar->menu(menu_mnu_bar);
    } // Fl_Menu_Bar* mnu_bar
    { myUI = new my_UI(0, 25, 390, 430);
      myUI->box(FL_NO_BOX);
      myUI->color(FL_BACKGROUND_COLOR);
      myUI->selection_color(FL_BACKGROUND_COLOR);
      myUI->labeltype(FL_NORMAL_LABEL);
      myUI->labelfont(0);
      myUI->labelsize(14);
      myUI->labelcolor(FL_FOREGROUND_COLOR);
      myUI->align(Fl_Align(FL_ALIGN_TOP));
      myUI->when(FL_WHEN_RELEASE);
      { txtNcallins = new Fl_Box(125, 25, 75, 20, "0 callins");
        txtNcallins->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
      } // Fl_Box* txtNcallins
      { net_grp1 = new Fl_Group(0, 45, 225, 255, "      Call In List");
        net_grp1->tooltip("Use UP/DN arrow keys to scroll list");
        net_grp1->labelfont(1);
        net_grp1->align(Fl_Align(FL_ALIGN_TOP_LEFT));
        { ptr_left = new Fl_Box(5, 125, 15, 25, "@>");
        } // Fl_Box* ptr_left
        { ptr_right = new Fl_Box(210, 125, 15, 25, "@<");
        } // Fl_Box* ptr_right
        { net_grp2 = new Fl_Group(17, 45, 196, 255);
          net_grp2->box(FL_DOWN_BOX);
          net_grp2->color(FL_BACKGROUND2_COLOR);
          { txtTitles = new Fl_Box(20, 47, 190, 22, "Call    Name    Time  F");
            txtTitles->box(FL_FLAT_BOX);
            txtTitles->color((Fl_Color)23);
            txtTitles->labelfont(4);
            txtTitles->labelsize(13);
            txtTitles->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtTitles
          { txtLine[0] = new Fl_Box(20, 70, 190, 15);
            txtLine[0]->box(FL_FLAT_BOX);
            txtLine[0]->color(FL_BACKGROUND2_COLOR);
            txtLine[0]->labelfont(4);
            txtLine[0]->labelsize(13);
            txtLine[0]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[0]
          { txtLine[1] = new Fl_Box(20, 85, 190, 15);
            txtLine[1]->box(FL_FLAT_BOX);
            txtLine[1]->color(FL_BACKGROUND2_COLOR);
            txtLine[1]->labelfont(4);
            txtLine[1]->labelsize(13);
            txtLine[1]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[1]
          { txtLine[2] = new Fl_Box(20, 100, 190, 15);
            txtLine[2]->box(FL_FLAT_BOX);
            txtLine[2]->color(FL_BACKGROUND2_COLOR);
            txtLine[2]->labelfont(4);
            txtLine[2]->labelsize(13);
            txtLine[2]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[2]
          { txtLine[3] = new Fl_Box(20, 115, 190, 15);
            txtLine[3]->box(FL_FLAT_BOX);
            txtLine[3]->color(FL_BACKGROUND2_COLOR);
            txtLine[3]->labelfont(4);
            txtLine[3]->labelsize(13);
            txtLine[3]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[3]
          { txtLine[4] = new Fl_Box(20, 130, 190, 15);
            txtLine[4]->box(FL_FLAT_BOX);
            txtLine[4]->color(FL_BACKGROUND2_COLOR);
            txtLine[4]->labelfont(4);
            txtLine[4]->labelsize(13);
            txtLine[4]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[4]
          { txtLine[5] = new Fl_Box(20, 145, 190, 15);
            txtLine[5]->box(FL_FLAT_BOX);
            txtLine[5]->color(FL_BACKGROUND2_COLOR);
            txtLine[5]->labelfont(4);
            txtLine[5]->labelsize(13);
            txtLine[5]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[5]
          { txtLine[6] = new Fl_Box(20, 160, 190, 15);
            txtLine[6]->box(FL_FLAT_BOX);
            txtLine[6]->color(FL_BACKGROUND2_COLOR);
            txtLine[6]->labelfont(4);
            txtLine[6]->labelsize(13);
            txtLine[6]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[6]
          { txtLine[7] = new Fl_Box(20, 175, 190, 15);
            txtLine[7]->box(FL_FLAT_BOX);
            txtLine[7]->color(FL_BACKGROUND2_COLOR);
            txtLine[7]->labelfont(4);
            txtLine[7]->labelsize(13);
            txtLine[7]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[7]
          { txtLine[8] = new Fl_Box(20, 190, 190, 15);
            txtLine[8]->box(FL_FLAT_BOX);
            txtLine[8]->color(FL_BACKGROUND2_COLOR);
            txtLine[8]->labelfont(4);
            txtLine[8]->labelsize(13);
            txtLine[8]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[8]
          { txtLine[9] = new Fl_Box(20, 205, 190, 15);
            txtLine[9]->box(FL_FLAT_BOX);
            txtLine[9]->color(FL_BACKGROUND2_COLOR);
            txtLine[9]->labelfont(4);
            txtLine[9]->labelsize(13);
            txtLine[9]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[9]
          { txtLine[10] = new Fl_Box(20, 220, 190, 15);
            txtLine[10]->box(FL_FLAT_BOX);
            txtLine[10]->color(FL_BACKGROUND2_COLOR);
            txtLine[10]->labelfont(4);
            txtLine[10]->labelsize(13);
            txtLine[10]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[10]
          { txtLine[11] = new Fl_Box(20, 235, 190, 15);
            txtLine[11]->box(FL_FLAT_BOX);
            txtLine[11]->color(FL_BACKGROUND2_COLOR);
            txtLine[11]->labelfont(4);
            txtLine[11]->labelsize(13);
            txtLine[11]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[11]
          { txtLine[12] = new Fl_Box(20, 250, 190, 15);
            txtLine[12]->box(FL_FLAT_BOX);
            txtLine[12]->color(FL_BACKGROUND2_COLOR);
            txtLine[12]->labelfont(4);
            txtLine[12]->labelsize(13);
            txtLine[12]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[12]
          { txtLine[13] = new Fl_Box(20, 265, 190, 15);
            txtLine[13]->box(FL_FLAT_BOX);
            txtLine[13]->color(FL_BACKGROUND2_COLOR);
            txtLine[13]->labelfont(4);
            txtLine[13]->labelsize(13);
            txtLine[13]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[13]
          { txtLine[14] = new Fl_Box(20, 280, 190, 15);
            txtLine[14]->box(FL_FLAT_BOX);
            txtLine[14]->color(FL_BACKGROUND2_COLOR);
            txtLine[14]->labelfont(4);
            txtLine[14]->labelsize(13);
            txtLine[14]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtLine[14]
          net_grp2->end();
        } // Fl_Group* net_grp2
        net_grp1->end();
      } // Fl_Group* net_grp1
      { dbSelectGroup = new Fl_Group(230, 45, 155, 250);
        dbSelectGroup->box(FL_ENGRAVED_FRAME);
        dbSelectGroup->align(Fl_Align(FL_ALIGN_TOP_LEFT));
        { inpLoginSuffix = new Fl_Box(240, 75, 40, 25);
          inpLoginSuffix->tooltip("Enter SUFFIX, Esc to abort");
          inpLoginSuffix->box(FL_DOWN_BOX);
          inpLoginSuffix->color(FL_BACKGROUND2_COLOR);
          inpLoginSuffix->labelfont(13);
          inpLoginSuffix->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
        } // Fl_Box* inpLoginSuffix
        { inpLoginPrefix = new Fl_Box(285, 75, 30, 25);
          inpLoginPrefix->tooltip("Enter PREFIX Esc to abort");
          inpLoginPrefix->box(FL_DOWN_BOX);
          inpLoginPrefix->color(FL_BACKGROUND2_COLOR);
          inpLoginPrefix->labelfont(13);
          inpLoginPrefix->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
        } // Fl_Box* inpLoginPrefix
        { inpLoginArea = new Fl_Box(320, 75, 40, 25);
          inpLoginArea->tooltip("Enter AREA Esc to abort");
          inpLoginArea->box(FL_DOWN_BOX);
          inpLoginArea->color(FL_BACKGROUND2_COLOR);
          inpLoginArea->labelfont(13);
          inpLoginArea->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
        } // Fl_Box* inpLoginArea
        { net_grp3 = new Fl_Group(240, 115, 75, 160);
          net_grp3->tooltip("Use UP/DN arrow keys to select callsign");
          net_grp3->box(FL_DOWN_BOX);
          net_grp3->color(FL_BACKGROUND2_COLOR);
          { txtPick[0] = new Fl_Box(240, 115, 75, 16);
            txtPick[0]->color(FL_BACKGROUND2_COLOR);
            txtPick[0]->labelfont(13);
            txtPick[0]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[0]
          { txtPick[1] = new Fl_Box(240, 131, 75, 16);
            txtPick[1]->color(FL_BACKGROUND2_COLOR);
            txtPick[1]->labelfont(13);
            txtPick[1]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[1]
          { txtPick[2] = new Fl_Box(240, 147, 75, 16);
            txtPick[2]->color(FL_BACKGROUND2_COLOR);
            txtPick[2]->labelfont(13);
            txtPick[2]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[2]
          { txtPick[3] = new Fl_Box(240, 163, 75, 16);
            txtPick[3]->color(FL_BACKGROUND2_COLOR);
            txtPick[3]->labelfont(13);
            txtPick[3]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[3]
          { txtPick[4] = new Fl_Box(240, 179, 75, 16);
            txtPick[4]->color(FL_BACKGROUND2_COLOR);
            txtPick[4]->labelfont(13);
            txtPick[4]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[4]
          { txtPick[5] = new Fl_Box(240, 195, 75, 16);
            txtPick[5]->color(FL_BACKGROUND2_COLOR);
            txtPick[5]->labelfont(13);
            txtPick[5]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[5]
          { txtPick[6] = new Fl_Box(240, 211, 75, 16);
            txtPick[6]->color(FL_BACKGROUND2_COLOR);
            txtPick[6]->labelfont(13);
            txtPick[6]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[6]
          { txtPick[7] = new Fl_Box(240, 227, 75, 16);
            txtPick[7]->color(FL_BACKGROUND2_COLOR);
            txtPick[7]->labelfont(13);
            txtPick[7]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[7]
          { txtPick[8] = new Fl_Box(240, 243, 75, 16);
            txtPick[8]->color(FL_BACKGROUND2_COLOR);
            txtPick[8]->labelfont(13);
            txtPick[8]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[8]
          { txtPick[9] = new Fl_Box(240, 259, 75, 16);
            txtPick[9]->color(FL_BACKGROUND2_COLOR);
            txtPick[9]->labelfont(13);
            txtPick[9]->align(Fl_Align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE));
          } // Fl_Box* txtPick[9]
          net_grp3->end();
        } // Fl_Group* net_grp3
        { txtPickArrows = new Fl_Box(320, 135, 25, 120);
          txtPickArrows->hide();
        } // Fl_Box* txtPickArrows
        { bx_suffix = new Fl_Box(235, 45, 40, 30, "Suffix");
        } // Fl_Box* bx_suffix
        { bx_prefix = new Fl_Box(280, 45, 40, 30, "Prefix");
        } // Fl_Box* bx_prefix
        { bx_area = new Fl_Box(320, 45, 40, 30, "Area");
        } // Fl_Box* bx_area
        dbSelectGroup->end();
      } // Fl_Group* dbSelectGroup
      myUI->end();
    } // my_UI* myUI
    { txtInfo = new Fl_Box(5, 301, 379, 150);
      txtInfo->box(FL_DOWN_BOX);
      txtInfo->color((Fl_Color)20);
      txtInfo->selection_color(FL_BACKGROUND2_COLOR);
      txtInfo->labelfont(5);
      txtInfo->align(Fl_Align(FL_ALIGN_TOP_LEFT|FL_ALIGN_INSIDE));
    } // Fl_Box* txtInfo
    o->end();
  } // Fl_Double_Window* o
  return w;
}

Fl_Tabs *tabsConfig=(Fl_Tabs *)0;

Fl_Check_Button *btn_new_login_is_up=(Fl_Check_Button *)0;

static void cb_btn_new_login_is_up(Fl_Check_Button* o, void*) {
  disp_new_login=o->value();
}

Fl_Check_Button *btnOpenEditor=(Fl_Check_Button *)0;

static void cb_btnOpenEditor(Fl_Check_Button* o, void*) {
  open_editor = o->value();
}

Fl_Check_Button *btn_current_call_in_is_up=(Fl_Check_Button *)0;

static void cb_btn_current_call_in_is_up(Fl_Check_Button* o, void*) {
  callin_is_up = o->value();
}

Fl_Group *tabGroupColors=(Fl_Group *)0;

static void cb_btnFg(Fl_Button*, void*) {
  fgColors[1] = fl_show_colormap(fgColors[1]);
txtSample[1]->textcolor(fgColors[1]);
txtSample[1]->redraw();
}

static void cb_btnBg(Fl_Button*, void*) {
  bgColors[1] = fl_show_colormap(bgColors[1]);
txtSample[1]->color(bgColors[1]);
txtSample[1]->redraw();
}

static void cb_btnFg1(Fl_Button*, void*) {
  fgColors[2] = fl_show_colormap(fgColors[2]);
txtSample[2]->textcolor(fgColors[2]);
txtSample[2]->redraw();
}

static void cb_btnBg1(Fl_Button*, void*) {
  bgColors[2] = fl_show_colormap(bgColors[2]);
txtSample[2]->color(bgColors[2]);
txtSample[2]->redraw();
}

static void cb_btnFg2(Fl_Button*, void*) {
  fgColors[3] = fl_show_colormap(fgColors[3]);
txtSample[3]->textcolor(fgColors[3]);
txtSample[3]->redraw();
}

static void cb_btnBg2(Fl_Button*, void*) {
  bgColors[3] = fl_show_colormap(bgColors[3]);
txtSample[3]->color(bgColors[3]);
txtSample[3]->redraw();
}

Fl_Output *txtSample[5]={(Fl_Output *)0};

Fl_Button *btnFg[5]={(Fl_Button *)0};

static void cb_btnFg3(Fl_Button*, void*) {
  fgColors[4] = fl_show_colormap(fgColors[4]);
txtSample[4]->textcolor(fgColors[4]);
txtSample[4]->redraw();
}

Fl_Button *btnBg[5]={(Fl_Button *)0};

static void cb_btnBg3(Fl_Button*, void*) {
  bgColors[4] = fl_show_colormap(bgColors[4]);
txtSample[4]->color(bgColors[4]);
txtSample[4]->redraw();
}

Fl_Group *tabGroupPriority=(Fl_Group *)0;

Fl_Input *cfgP1=(Fl_Input *)0;

Fl_Input *inpStatesList1=(Fl_Input *)0;

Fl_Input *cfgP2=(Fl_Input *)0;

Fl_Input *inpStatesList2=(Fl_Input *)0;

Fl_Input *cfgP3=(Fl_Input *)0;

Fl_Input *inpStatesList3=(Fl_Input *)0;

Fl_Check_Button *chkAutoPriority=(Fl_Check_Button *)0;

Fl_Return_Button *btnConfigOK=(Fl_Return_Button *)0;

Fl_Double_Window* configDialog() {
  Fl_Double_Window* w;
  { Fl_Double_Window* o = new Fl_Double_Window(440, 275, "Net Configuration");
    w = o;
    { tabsConfig = new Fl_Tabs(5, 10, 430, 210);
      tabsConfig->color((Fl_Color)44);
      { Fl_Group* o = new Fl_Group(5, 35, 430, 185, "UI behavior");
        { Fl_Check_Button* o = btn_new_login_is_up = new Fl_Check_Button(30, 60, 70, 15, "New login is up");
          btn_new_login_is_up->tooltip("Move new login to the >...<\nspot in the calll in list");
          btn_new_login_is_up->down_box(FL_DOWN_BOX);
          btn_new_login_is_up->callback((Fl_Callback*)cb_btn_new_login_is_up);
          o->value(disp_new_login);
        } // Fl_Check_Button* btn_new_login_is_up
        { Fl_Check_Button* o = btnOpenEditor = new Fl_Check_Button(60, 102, 70, 15, "Open editor for new login");
          btnOpenEditor->tooltip("Open editor for new call in\nNew login is up must be enabled");
          btnOpenEditor->down_box(FL_DOWN_BOX);
          btnOpenEditor->callback((Fl_Callback*)cb_btnOpenEditor);
          o->value(open_editor);
        } // Fl_Check_Button* btnOpenEditor
        { Fl_Check_Button* o = btn_current_call_in_is_up = new Fl_Check_Button(30, 145, 70, 15, "Current call in is up");
          btn_current_call_in_is_up->tooltip("Move last login to the >...<\nspot in the calll in list");
          btn_current_call_in_is_up->down_box(FL_DOWN_BOX);
          btn_current_call_in_is_up->callback((Fl_Callback*)cb_btn_current_call_in_is_up);
          o->value(callin_is_up);
        } // Fl_Check_Button* btn_current_call_in_is_up
        o->end();
      } // Fl_Group* o
      { tabGroupColors = new Fl_Group(5, 45, 430, 160, "Colors");
        tabGroupColors->hide();
        { Fl_Output* o = txtSample[1] = new Fl_Output(135, 60, 45, 25, "Logged In");
          txtSample[1]->textfont(13);
          o->value("Text");
          o->color(bgColors[1]);
          o->textcolor(fgColors[1]);
        } // Fl_Output* txtSample[1]
        { btnFg[1] = new Fl_Button(195, 60, 45, 25, "Fg");
          btnFg[1]->callback((Fl_Callback*)cb_btnFg);
        } // Fl_Button* btnFg[1]
        { btnBg[2] = new Fl_Button(260, 60, 45, 25, "Bg");
          btnBg[2]->callback((Fl_Callback*)cb_btnBg);
        } // Fl_Button* btnBg[2]
        { Fl_Output* o = txtSample[2] = new Fl_Output(135, 95, 45, 25, "First Response");
          txtSample[2]->textfont(13);
          o->value("Text");
          o->color(bgColors[2]);
          o->textcolor(fgColors[2]);
        } // Fl_Output* txtSample[2]
        { btnFg[2] = new Fl_Button(195, 95, 45, 25, "Fg");
          btnFg[2]->callback((Fl_Callback*)cb_btnFg1);
        } // Fl_Button* btnFg[2]
        { btnBg[2] = new Fl_Button(260, 95, 45, 25, "Bg");
          btnBg[2]->callback((Fl_Callback*)cb_btnBg1);
        } // Fl_Button* btnBg[2]
        { Fl_Output* o = txtSample[3] = new Fl_Output(135, 130, 45, 25, "Second Response");
          txtSample[3]->textfont(13);
          o->value("Text");
          o->color(bgColors[3]);
          o->textcolor(fgColors[3]);
        } // Fl_Output* txtSample[3]
        { btnFg[3] = new Fl_Button(195, 130, 45, 25, "Fg");
          btnFg[3]->callback((Fl_Callback*)cb_btnFg2);
        } // Fl_Button* btnFg[3]
        { btnBg[3] = new Fl_Button(260, 130, 45, 25, "Bg");
          btnBg[3]->callback((Fl_Callback*)cb_btnBg2);
        } // Fl_Button* btnBg[3]
        { Fl_Output* o = txtSample[4] = new Fl_Output(135, 165, 45, 25, "Logged Out");
          txtSample[4]->textfont(13);
          o->value("Text");
          o->color(bgColors[4]);
          o->textcolor(fgColors[4]);
        } // Fl_Output* txtSample[4]
        { btnFg[4] = new Fl_Button(195, 165, 45, 25, "Fg");
          btnFg[4]->callback((Fl_Callback*)cb_btnFg3);
        } // Fl_Button* btnFg[4]
        { btnBg[4] = new Fl_Button(260, 165, 45, 25, "Bg");
          btnBg[4]->callback((Fl_Callback*)cb_btnBg3);
        } // Fl_Button* btnBg[4]
        tabGroupColors->end();
      } // Fl_Group* tabGroupColors
      { tabGroupPriority = new Fl_Group(5, 35, 430, 185, "Priority");
        tabGroupPriority->hide();
        { cfgP1 = new Fl_Input(160, 70, 20, 25, "Priority 1 character");
        } // Fl_Input* cfgP1
        { inpStatesList1 = new Fl_Input(200, 70, 225, 25, "States List (ie: FL, AL, GA)");
          inpStatesList1->align(Fl_Align(FL_ALIGN_TOP_LEFT));
        } // Fl_Input* inpStatesList1
        { cfgP2 = new Fl_Input(160, 100, 20, 25, "Priority 2 character");
        } // Fl_Input* cfgP2
        { inpStatesList2 = new Fl_Input(200, 100, 225, 25);
          inpStatesList2->align(Fl_Align(FL_ALIGN_TOP_LEFT));
        } // Fl_Input* inpStatesList2
        { cfgP3 = new Fl_Input(160, 130, 20, 25, "Priority 3 character");
        } // Fl_Input* cfgP3
        { inpStatesList3 = new Fl_Input(200, 130, 225, 25);
          inpStatesList3->align(Fl_Align(FL_ALIGN_TOP_LEFT));
        } // Fl_Input* inpStatesList3
        { chkAutoPriority = new Fl_Check_Button(160, 165, 25, 25, "Auto By Priority");
          chkAutoPriority->down_box(FL_DOWN_BOX);
          chkAutoPriority->align(Fl_Align(FL_ALIGN_LEFT));
        } // Fl_Check_Button* chkAutoPriority
        tabGroupPriority->end();
      } // Fl_Group* tabGroupPriority
      tabsConfig->end();
    } // Fl_Tabs* tabsConfig
    { btnConfigOK = new Fl_Return_Button(355, 235, 75, 25, "OK");
      btnConfigOK->callback((Fl_Callback*)cb_btnCloseConfig);
    } // Fl_Return_Button* btnConfigOK
    o->end();
  } // Fl_Double_Window* o
  return w;
}

Fl_Browser *log_in_view=(Fl_Browser *)0;

Fl_Button *btn_copy_to_clipboard=(Fl_Button *)0;

static void cb_btn_copy_to_clipboard(Fl_Button*, void*) {
  copy_to_clipboard();
}

Fl_Button *btn_close_log_ins=(Fl_Button *)0;

static void cb_btn_close_log_ins(Fl_Button* o, void*) {
  o->parent()->hide();
}

Fl_Double_Window* Log_ins_dialog() {
  Fl_Double_Window* w;
  { Fl_Double_Window* o = new Fl_Double_Window(240, 235, "Current Log Ins");
    w = o;
    { log_in_view = new Fl_Browser(0, 0, 240, 205);
      log_in_view->align(Fl_Align(FL_ALIGN_TOP));
    } // Fl_Browser* log_in_view
    { btn_copy_to_clipboard = new Fl_Button(30, 210, 70, 20, "Copy");
      btn_copy_to_clipboard->callback((Fl_Callback*)cb_btn_copy_to_clipboard);
    } // Fl_Button* btn_copy_to_clipboard
    { btn_close_log_ins = new Fl_Button(140, 210, 70, 20, "Close");
      btn_close_log_ins->callback((Fl_Callback*)cb_btn_close_log_ins);
    } // Fl_Button* btn_close_log_ins
    o->end();
  } // Fl_Double_Window* o
  return w;
}
